import Vue from "vue";
import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://localhost:5000/api"
});

Vue.prototype.$axios = axios;

export { axiosInstance };
