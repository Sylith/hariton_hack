import { API_URL } from "../../urls";
import axios from "axios";

export default {
  namespaced: true,
  actions: {
    // get all tasks, home page
    async fetchDoctor(ctx) {
      try {
        const res = await axios.get(API_URL + "doctors/me", {
          headers: {
            Authorization: localStorage.getItem("Authorization")
          }
        });
        ctx.commit("updateDoctor", {
          doctor: res.data
        });
      } catch (error) {
        console.log(error);
      }
    }
    // search by ingridients
    // async fetchInhg(ctx) {},

    // async fetchFavs(ctx) {
    //   if (localStorage.getItem("Authorization")) {
    //     const res = await fetch(API_URL + "/users/favourites", {
    //       method: "GET",
    //       headers: {
    //         Authorization: localStorage.getItem("Authorization")
    //       }
    //     });
    //     const favs = await res.json();
    //     ctx.commit("updateFavs", {
    //       favs: favs.favourites
    //     });
    //   }
    // },

    // async fetchAllFavs(ctx, payload) {
    //   const res = await fetch(
    //     API_URL + "/recipes/all/favourites" + payload.queryString,
    //     {
    //       headers: {
    //         "Accept-Language": "en",
    //         Authorization: localStorage.getItem("Authorization")
    //       }
    //     }
    //   );
    //   const recipes = await res.json();
    //   ctx.commit("updateFavsRecipes", {
    //     recipes: recipes.data,
    //     pages: recipes.pages
    //   });
    // }
  },
  mutations: {
    updateDoctor(state, doctor) {
      state.doctor = doctor.doctor;
    }
    // updateFavsRecipes(state, payload) {
    //   state.favsRecipes = payload.recipes;
    //   state.favsRecipesPages = payload.pages;
    // },
    // updateFavs(state, payload) {
    //   state.favs = payload.favs;
    // }
  },
  state: {
    doctor: {}
  },
  getters: {
    getDoctor: state => {
      return state.doctor;
      //   return state.tasks;
    }
    // allFavsRecipesGetter(state) {
    //   return state.favsRecipes;
    // }
    // isFavGetter: (state) => (payload) => {
    //     if (
    //         state.favs.some(
    //             (el) =>
    //                 el._id === payload._id &&
    //                 el.language === payload.language
    //         )
    //     ) {
    //         return true
    //     } else {
    //         return false
    //     }
    // },
  }
};
