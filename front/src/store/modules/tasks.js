import { API_URL } from "../../urls";
import axios from "axios";

export default {
  namespaced: true,
  actions: {
    // get all tasks, home page
    async fetchAll(ctx) {
      try {
        const res = await axios.get(API_URL + "users/all");
        ctx.commit("updateAll", {
          tasks: res.data
        });
      } catch (error) {
        console.log(error);
      }
    },
    async fetchById(ctx, id) {
      try {
        const res = await axios.get(API_URL + "users/find/" + id, {
          headers: {
            Authorization: localStorage.getItem("Authorization")
          }
        });
        ctx.commit("updateTask", {
          task: res.data
        });
      } catch (error) {
        console.log(error);
      }
    }
    // search by ingridients
    // async fetchInhg(ctx) {},

    // async fetchFavs(ctx) {
    //   if (localStorage.getItem("Authorization")) {
    //     const res = await fetch(API_URL + "/users/favourites", {
    //       method: "GET",
    //       headers: {
    //         Authorization: localStorage.getItem("Authorization")
    //       }
    //     });
    //     const favs = await res.json();
    //     ctx.commit("updateFavs", {
    //       favs: favs.favourites
    //     });
    //   }
    // },

    // async fetchAllFavs(ctx, payload) {
    //   const res = await fetch(
    //     API_URL + "/recipes/all/favourites" + payload.queryString,
    //     {
    //       headers: {
    //         "Accept-Language": "en",
    //         Authorization: localStorage.getItem("Authorization")
    //       }
    //     }
    //   );
    //   const recipes = await res.json();
    //   ctx.commit("updateFavsRecipes", {
    //     recipes: recipes.data,
    //     pages: recipes.pages
    //   });
    // }
  },
  mutations: {
    updateAll(state, tasks) {
      state.tasks = tasks.tasks;
    },
    updateTask(state, task) {
      state.task = task.task;
    }
    // updateFavsRecipes(state, payload) {
    //   state.favsRecipes = payload.recipes;
    //   state.favsRecipesPages = payload.pages;
    // },
    // updateFavs(state, payload) {
    //   state.favs = payload.favs;
    // }
  },
  state: {
    tasks: [],
    task: {}
  },
  getters: {
    allTasks: state => ({ page, elems }) => {
      return state.tasks.slice((page - 1) * elems, page * elems);
      //   return state.tasks;
    },
    allTasksSize: state => {
      return state.tasks.length;
    },
    getTask: state => {
      return state.task;
    }
    // allFavsRecipesGetter(state) {
    //   return state.favsRecipes;
    // }
    // isFavGetter: (state) => (payload) => {
    //     if (
    //         state.favs.some(
    //             (el) =>
    //                 el._id === payload._id &&
    //                 el.language === payload.language
    //         )
    //     ) {
    //         return true
    //     } else {
    //         return false
    //     }
    // },
  }
};
