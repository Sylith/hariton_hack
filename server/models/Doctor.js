const { Schema, model } = require('mongoose')

const schema = new Schema(
  {
    name: {
      type: String,
      default: '',
      min: 2,
      max: 50,
    },
    email: {
      type: String,
      required: true,
      min: 4,
    },
    vkID: {
      type: String,
      default: '',
    },
    tgID: {
      type: String,
      default: '',
    },
    status: {
      type: String,
      default: '',
    },
    password: {
      type: String,
      required: true,
      min: 6,
      max: 1024,
    },
    patientID: {
      type: String,
      default: '',
    },
    qualifications: {
      //   minAge: {
      //     type: Number,
      //     required: true,
      //     default: 0,
      //   },
      //   maxAge: {
      //     type: Number,
      //     required: true,
      //     default: 18,
      //   },
      //   expirience: {
      //     type: Number,
      //     required: true,
      //     default: 0,
      //   },
      type: Schema.Types.Mixed,
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
)

module.exports = model('Doctor', schema)
