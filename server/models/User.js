const { Schema, model } = require('mongoose')

const schema = new Schema(
  {
    platform: {
      type: String,
      required: true,
    },
    userID: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      default: '',
    },
    additional: {
      type: String,
      default: '',
    },
    hasTask: {
      type: Boolean,
      default: false,
    },
    task: {
      description: {
        type: String,
        default: '',
      },
      qualifications: {
        type: Object,
        default: {},
      },
      doctorsID: {
        type: String,
        default: '',
      },
    },
  },
  { timestamps: true }
)

module.exports = model('User', schema)
