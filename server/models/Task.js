const { Schema, model } = require('mongoose')

const schema = new Schema(
  {
    description: {
      type: String,
      required: true,
    },
    qualifications: {
      //   minAge: {
      //     type: Number,
      //     required: true,
      //     default: 0,
      //   },
      //   maxAge: {
      //     type: Number,
      //     required: true,
      //     default: 18,
      //   },
      //   expirience: {
      //     type: Number,
      //     required: true,
      //     default: 0,
      //   },
      type: Object,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    isDone: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
)

module.exports = model('Task', schema)
