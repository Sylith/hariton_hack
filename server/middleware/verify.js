const jwt = require('jsonwebtoken')

function auth(req, res, next) {
  const token = req.header('Authorization')
  if (!token) return res.status(401).send({ Error: 'Auth required' })

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET)
    req.doctor = verified
    next()
  } catch (error) {
    res.status(400).send({ Error: 'Invalid token' })
  }
}

module.exports = auth
