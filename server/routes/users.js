const { Router } = require('express')
const Doctor = require('../models/Doctor')
const User = require('../models/USer')
const auth = require('../middleware/verify')

const router = Router()

// ADMIN ONLY
router.post('/new', auth, async (req, res) => {
  if (!req.doctor.isAdmin)
    return res.status(403).send({ Error: 'Access denied' })

  const user = new User({
    platform: req.body.platform,
    userID: req.body.userID,
    status: req.body.status || '',
    additional: req.body.additional || '',
    hasTask: req.body.hasTask,
    task: {
      description: req.body.task.description,
      qualifications: req.body.task.qualifications,
      doctorsID: req.body.task.doctorsID,
    },
  })
  try {
    const savedUser = await user.save()
    res.send(savedUser)
  } catch (error) {
    res.status(400).send(error)
  }
})

// ADMIN ONLY
router.delete('/delete', auth, async (req, res) => {
  if (!req.doctor.isAdmin)
    return res.status(403).send({ Error: 'Access denied' })

  try {
    const userDelete = await User.findByIdAndDelete(req.body.id)
    if (!userDelete) return res.status(400).send({ Error: 'No such task' })
    res.send(userDelete)
  } catch (error) {
    res.status(400).send({ Error: 'Failed to delete' })
  }
})

// get all users with tasks
router.get('/all', async (req, res) => {
  try {
    // const users = await User.find({}, { _id: 1, task: 1 })
    const users = await User.find()
    if (users.length === 0)
      return res.status(400).send({ Error: 'No task yet' })
    res.send(users)
  } catch (error) {
    res.status(400).send({ Error: 'No task yet' })
  }
})

router.get('/find/:id', auth, async (req, res) => {
  try {
    const user = await User.findById(req.params.id)
    if (!user) return res.status(400).send({ Error: 'No such user' })
    res.send(user)
  } catch (error) {
    console.log(error)
    res.status(400).send({ Error: 'No such user' })
  }
})

//////////// TODO: right check (get task refered to doctor)
router.get('/me', auth, async (req, res) => {
  const doctor = await Doctor.findById(req.doctor._id)
  //   const tasks = await Task.find({ qualifications: doctor.qualifications })
  const users = await User.find()
  if (!users)
    return res
      .status(400)
      .send({ Error: 'No task refered to your qualifiaction' })
})

// Take task from doctor
router.put('/take_task', auth, async (req, res) => {
  try {
    const user = await User.findById(req.body.id)
    const doctor = await Doctor.findById(req.doctor._id)
    if (doctor.patientID !== '')
      return res
        .status(400)
        .send({ Error: 'You are currently working with another user' })
    if (!user) return res.status(400).send({ Error: 'No such user' })
    if (!user.hasTask)
      return res.status(400).send({ Error: "User doesn't have task" })
    if (user.task.doctorsID !== '')
      return res.status(400).send({ Error: 'Task is already taken' })
    const updatedTask = await User.findByIdAndUpdate(
      req.body.id,
      {
        $set: {
          'task.doctorsID': req.doctor._id,
        },
      },
      { new: 'true' }
    )
    try {
      const updatedDoctor = await Doctor.findByIdAndUpdate(req.doctor._id, {
        patientID: req.body.id,
      })
    } catch (error) {
      console.log(error)
      res.status(400).send({ Error: 'Failed to update Doctor' })
    }
    res.send(updatedTask)
  } catch (error) {
    console.log(error)
    return res.status(400).send({ Error: 'Failed to update User' })
  }
})

// Decline task from doctor
router.put('/abort_task', auth, async (req, res) => {
  try {
    const user = await User.findById(req.body.id)
    if (!user) return res.status(400).send({ Error: 'No such user' })
    if (!user.hasTask)
      return res.status(400).send({ Error: "User doesn't have task" })
    if (user.task.doctorsID === '')
      return res.status(400).send({ Error: 'Task is already aborted' })
    if (user.task.doctorsID !== req.doctor._id)
      return res.status(400).send({ Error: 'Not your task!' })
    const updatedTask = await User.findByIdAndUpdate(
      req.body.id,
      {
        $set: {
          'task.doctorsID': '',
        },
      },
      { new: 'true' }
    )
    try {
      const updatedDoctor = await Doctor.findByIdAndUpdate(req.doctor._id, {
        $set: {
          patientID: '',
        },
      })
    } catch (error) {
      console.log(error)
      res.status(400).send({ Error: 'Failed to update Doctor' })
    }
    res.send(updatedTask)
  } catch (error) {
    console.log(error)
    return res.status(400).send({ Error: 'Failed to update User' })
  }
})

module.exports = router
