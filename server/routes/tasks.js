const { Router } = require('express')
const Doctor = require('../models/Doctor')
const Task = require('../models/Task')
const auth = require('../middleware/verify')

const router = Router()

// ADMIN ONLY
router.post('/new', auth, async (req, res) => {
  if (!req.doctor.isAdmin)
    return res.status(403).send({ Error: 'Access denied' })

  const task = new Task({
    description: req.body.description,
  })
  try {
    const savedTask = await task.save()
    res.send(savedTask)
  } catch (error) {
    res.status(400).send(error)
  }
})

// ADMIN ONLY
router.delete('/delete', auth, async (req, res) => {
  if (!req.doctor.isAdmin)
    return res.status(403).send({ Error: 'Access denied' })

  try {
    const taskDelete = await Task.findByIdAndDelete(req.body.id)
    if (!taskDelete) return res.status(400).send({ Error: 'No such task' })
    res.send(taskDelete)
  } catch (error) {
    res.status(400).send({ Error: 'Failed to delete' })
  }
})

router.get('/all', async (req, res) => {
  try {
    const tasks = await Task.find()
    if (tasks.length === 0)
      return res.status(400).send({ Error: 'No task yet' })
    res.send(tasks)
  } catch (error) {
    res.status(400).send({ Error: 'No task yet' })
  }
})

//////////// TODO: right check
router.get('/me', auth, async (req, res) => {
  const doctor = await Doctor.findById(req.doctor._id)
  //   const tasks = await Task.find({ qualifications: doctor.qualifications })
  const tasks = await Task.find()
  if (!tasks)
    return res
      .status(400)
      .send({ Error: 'No task refered to your qualifiaction' })
})

///// TOGGLE Properly
router.put('/is_active_toggle', auth, async (req, res) => {
  try {
    const isActive = await Task.findById(req.body.id, { isActive: 1 })
    const task = await Task.findByIdAndUpdate(
      req.body.id,
      {
        $set: {
          isActive: !isActive.isActive,
        },
      },
      { new: 'true' }
    )
    res.send(task)
  } catch (error) {
    return res.status(400).send({ Error: 'No such task' })
  }
})

router.put('/is_done_toggle', auth, async (req, res) => {
  try {
    const isDone = await Task.findById(req.body.id, { isDone: 1 })
    const task = await Task.findByIdAndUpdate(
      req.body.id,
      {
        $set: {
          isDone: !isDone.isDone,
        },
      },
      { new: 'true' }
    )
    res.send(task)
  } catch (error) {
    return res.status(400).send({ Error: 'No such task' })
  }
})

module.exports = router
