const { Router } = require('express')
const Doctor = require('../models/Doctor')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/verify')
const { route } = require('./tasks')

const router = Router()

// Get all doctors
router.get('/all', async (req, res) => {
  try {
    const doctors = await Doctor.find()
    res.send(doctors)
  } catch (error) {
    res.send({ Error: 'No doctors yet' })
  }
})

// Registration of Doctor
router.post('/register', async (req, res) => {
  // user exists?
  const emailExist = await Doctor.findOne({ email: req.body.email })
  if (emailExist) return res.status(400).send({ Error: 'User alredy exist' })

  // password hash
  const salt = await bcrypt.genSalt(10)
  const hashesPassword = await bcrypt.hash(req.body.password, salt)

  const doctor = new Doctor({
    // name: req.body.name,
    email: req.body.email,
    password: hashesPassword,
    // qualifications: req.body.qualifications,
  })

  try {
    const savedDoctor = await doctor.save()
    const token = jwt.sign(
      {
        _id: savedDoctor._id,
        isAdmin: savedDoctor.isAdmin,
        // name: savedDoctor.name,
      },
      process.env.TOKEN_SECRET
    )
    res.header('Authorization', token).send(token)
  } catch (error) {
    res.status(400).send(error)
  }
})

// Login
router.post('/login', async (req, res) => {
  if (!req.body.email) return res.status(400).send({ Error: 'No email' })
  if (!req.body.password) return res.status(400).send({ Error: 'No password' })

  // check if the user exists
  const doctor = await Doctor.findOne({ email: req.body.email })
  if (!doctor) return res.status(400).send({ Error: 'Wrong email' })

  // check password
  const passwordValid = await bcrypt.compare(req.body.password, doctor.password)
  if (!passwordValid) return res.status(400).send({ Error: 'Invalid password' })

  // create jwt
  const token = jwt.sign(
    { _id: doctor._id, isAdmin: doctor.isAdmin },
    process.env.TOKEN_SECRET
  )
  res.header('Authorization', token).send(token)
})

// Doctor info
router.get('/me', auth, async (req, res) => {
  try {
    const doctor = await Doctor.findById(req.doctor._id, {
      password: 0,
      isAdmin: 0,
    })

    res.send(doctor)
  } catch (error) {
    console.log(error)
  }
})

// Update info about doctor
router.put('/update_me', auth, async (req, res) => {
  const oldDoctor = await Doctor.findById(req.doctor._id, {
    _id: 1,
    email: 1,
    password: 1,
  })
  let modifications = req.body.doctor
  if (!req.doctor.isAdmin) modifications.isAdmin = false
  const toUpdate = Object.assign(oldDoctor, modifications)
  try {
    const newDoctor = await Doctor.findByIdAndUpdate(req.doctor._id, toUpdate, {
      new: 'true',
    })
    res.send(newDoctor)
  } catch (error) {
    res.status(400).send({ Error: 'Failed to update doctor' })
  }
})

// Delete myself
router.delete('/delete_me', auth, async (req, res) => {
  try {
    const doctorDelete = await Task.findByIdAndDelete(req.doctor._id)
    if (!doctorDelete) return res.status(400).send({ Error: 'No such doctor' })
    res.send(doctorDelete)
  } catch (error) {
    res.status(400).send({ Error: 'Failed to delete' })
  }
})

// Admin can delete other doctors
router.delete('/delete_from_admin', auth, async (req, res) => {
  if (!req.doctor.isAdmin)
    return res.status(403).send({ Error: 'Access denied' })
  try {
    const doctorDelete = await Task.findByIdAndDelete(req.body.id)
    if (!doctorDelete) return res.status(400).send({ Error: 'No such doctor' })
    res.send(doctorDelete)
  } catch (error) {
    res.status(400).send({ Error: 'Failed to delete' })
  }
})

module.exports = router
