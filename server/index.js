const express = require('express')

const mongoose = require('mongoose')
const cors = require('cors')
const dotenv = require('dotenv')

//routes
const doctorRoutes = require('./routes/doctors')
const userRoutes = require('./routes/users')

const app = express()

dotenv.config()

const PORT = process.env.PORT || 5000

// body parser middleware
app.use(express.urlencoded({ limit: '16mb', extended: true }))
app.use(express.json({ limit: '16mb', extended: true }))

app.use(cors())

// routes middleware
app.use('/api/doctors', doctorRoutes)
app.use('/api/users', userRoutes)

async function start() {
  try {
    await mongoose.connect(process.env.DB_CONNECT, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    })
    app.listen(PORT, () =>
      console.log(`Server started at http://localhost:${PORT}`)
    )
  } catch (error) {
    console.log(error)
  }
}

start()
